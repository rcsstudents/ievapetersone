﻿using System;
using System.Collections.Generic;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            List<MyDataTypes> myDataTypesList = new List<MyDataTypes>();

            Console.WriteLine("izpildiet programmu? (y / n)");
            while (Console.ReadLine() != "n")
            {
                MyDataTypes myDataTypes = new MyDataTypes();
                //1.Bool
                Console.WriteLine("ievadiet bool vertibu (true/false)");
                string myBoolString = Console.ReadLine();
                myDataTypes.Mybool = Convert.ToBoolean(myBoolString);

                //2.Byte
                Console.WriteLine("ievadiet byte vertibu (true/false)");
                string myByte = Console.ReadLine();

                bool isConverted = byte.TryParse(myBoolString, out byte result);
                if (isConverted == true)
                {
                    myDataTypes.Mybyte = Convert.ToByte(myByte);

                }

                //3.Short
                Console.WriteLine("ievadiet short vertibu (true/false)");
                string myShort = Console.ReadLine();
                myDataTypes.Myshort = Convert.ToInt16(myShort);

                //4.Int
                Console.WriteLine("ievadiet int vertibu (true/false)");
                string myInt = Console.ReadLine();
                myDataTypes.Myint = Convert.ToInt16(myShort);

                //5.Long
                Console.WriteLine("ievadiet long vertibu (true/false)");
                string myLong = Console.ReadLine();
                myDataTypes.Mylong = Convert.ToInt32(myShort);

                //6.Double
                Console.WriteLine("ievadiet double vertibu (true/false)");
                string myDouble = Console.ReadLine();
                myDataTypes.Mydouble = Convert.ToInt64(myShort);

                //7.Decimal
                Console.WriteLine("ievadiet decimal vertibu (true/false)");
                string myDecimal = Console.ReadLine();
                myDataTypes.Mydecimal = Convert.ToDecimal(myShort);

                //8.String
                Console.WriteLine("ievadiet string vertibu (true/false)");
                string myString = Console.ReadLine();
                myDataTypes.Mystring = Convert.ToString(myString);

                //9.char
                Console.WriteLine("ievadiet char vertibu (true/false)");
                string myChar = Console.ReadLine();
                myDataTypes.Mychar = Convert.ToChar(myChar);

                myDataTypesList.Add(myDataTypes);
            }

            foreach (MyDataTypes myData in myDataTypesList)
            {
                Console.WriteLine("MyBool =" + myData.Mybool);
            }

            Console.ReadKey();

        }
    }
}
