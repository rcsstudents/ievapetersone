﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public class MyDataTypes
    {
        public bool Mybool { get; set; }
        public byte Mybyte { get; set; }
        public short Myshort { get; set; }
        public int Myint { get; set; }
        public long Mylong { get; set; }
        public double Mydouble { get; set; }
        public decimal Mydecimal { get; set; }
        public string Mystring { get; set; }
        public char Mychar { get; set; }
    }
}
