﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Day1
{
    public static class DefinedQuestions
    {
        public static List<QuizQuestions> quizQuestions = new List<QuizQuestions>
        {
            new QuizQuestions
            {
                Question = "Kada ir tava milaka programmesanas valoda?",
                Answer ="C#"
            },
            new QuizQuestions
            {
                Question = "Kada ir tava milaka filma?",
                Answer ="ALX"
            },
            new QuizQuestions
            {
                Question = "Kas tev garso vislabak?",
                Answer = "Saldejums"
            }
        };
    }
}
