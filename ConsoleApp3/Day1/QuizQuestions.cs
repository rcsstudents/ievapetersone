﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Day1
{
    public class QuizQuestions
    {
        public string Question { get; set; }
        public string Answer { get; set; }
    }
}
