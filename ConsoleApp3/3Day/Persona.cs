﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _3Day
{
    public class Persona
    {
        public string Name;
        public double Height;
        public double Weight;
        public string HairColor;
        public DayOfWeek FavoriteWeekDay;

        public Persona(string name, double height, 
            double weight, string hairColor, DayOfWeek favoriteWeekDay)
        {
            Name = name;
            Height = height;
            Weight = weight;
            HairColor = hairColor;
            FavoriteWeekDay = favoriteWeekDay;
        }

        public void WritePersonInfo()
        {
            Console.WriteLine("" + Name + "");
            Console.WriteLine($"Persona {Name} ir {Height} cm auguma un svars {Weight} kg");
            Console.WriteLine($"Matu krasa ir {HairColor}");
            Console.WriteLine($"Milaka nedelas diena {FavoriteWeekDay}");
        }
    }
}
