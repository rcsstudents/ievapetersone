﻿using System;

namespace _3Day
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Ievadi skaitli no 1-7");
            string nedelasDiena = Console.ReadLine();
            int nedelasDienasNummurs = Convert.ToInt32(nedelasDiena);

            //1.Lietotajas ievada datumu
            int diena =1, menesis = 2, gads = 2001;

            //2.Izveido datuma objektu
            DateTime datums = new DateTime(gads, menesis, diena);

            //3.Nolasit DayOfWeek no datums
            //int dayOfWeek = (int)datums.DayOfWeek;
            DayOfWeek dayOfWeek = datums.DayOfWeek;

            //4.
            switch (nedelasDienasNummurs)
            {
                case (int)DayOfWeekEnum.Monday:
                    Console.WriteLine("Si ir pirmdiena");
                    break;
                default:
                    Console.WriteLine("Diena netika atrasta");
                    break;
            }

            if(dayOfWeek == DayOfWeek.Saturday || dayOfWeek == DayOfWeek.Sunday)
            {
                Console.WriteLine("Si ir drivdiena");
            }
            else
            {
                Console.WriteLine("Si ir darbadiena");
            }
            Console.ReadKey();
        }
    }
}
