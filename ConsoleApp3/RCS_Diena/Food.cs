﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RCS_3Diena
{
    public abstract class Food
    {
        //nesatur funkcionalitati
        //obligati vajag override so metodi
        
        //public abstract void DrinkAbstract();

        //satur funkcionalitati
        //var override so metodi, ja nepieciesams
        public virtual void Drink()
        {
            Console.WriteLine("I like coffee");
        }
        public void Eat()
        {
            Console.WriteLine("I like food");
        }
    }
}
