﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RCS_3Diena
{
    public class Person
    {
        public string Name;
        public double Height;
        public double Weight;
        public string HairColor;
        private DayOfWeek FavoriteWeekDay;
        private string vards;
        private double augumsDouble;
        private double svarsDouble;
        private string matuKrasa;
        private DayOfWeekEnum nedelasDienas;

        public Person(string name, double height, double weight, string hairColor, DayOfWeek favoriteWeekDay)
        {
            Name = name;
            Height = height;
            Weight = weight;
            HairColor = hairColor;
            FavoriteWeekDay = favoriteWeekDay;
        }

        public Person(string vards, double augumsDouble, double svarsDouble, string matuKrasa, DayOfWeekEnum nedelasDienas)
        {
            this.vards = vards;
            this.augumsDouble = augumsDouble;
            this.svarsDouble = svarsDouble;
            this.matuKrasa = matuKrasa;
            this.nedelasDienas = nedelasDienas;
        }

        public void WritePersonInfo()
        {
            Console.WriteLine($"Persona {Name} ir {Height} cm augums un svars {Weight} kg.*");
            Console.WriteLine($"Matu krasa {HairColor}");
            Console.WriteLine($"Milaka nedelas diena ir {FavoriteWeekDay}");
        }

        public void DoWalking()
        {
            Console.WriteLine("persona nogaja 30km");
        }

        public override void Drink()
        {
            Console.WriteLine("")
        }
    }
}