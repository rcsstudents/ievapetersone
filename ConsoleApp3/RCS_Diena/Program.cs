﻿using RCS_3Diena;
using System;
using System.Collections.Generic;

namespace RCS_Diena
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Ievadiet skaitli no diena" );
            string diena = Console.ReadLine();
            int dienasNummurs = Convert.ToInt32(diena);

            Console.WriteLine("Ievadiet skaitli no menesis" );
            string menesis = Console.ReadLine();
            int menesaNummurs = Convert.ToInt32(menesis);

            Console.WriteLine("Ievadiet skaitli no gads" );
            string gads = Console.ReadLine();
            int gadaNummurs = Convert.ToInt32(gads);

            //1.lietotajs ievada datumu
            //int diena = 1, menesis = 2, gads = 2018;

            //2.Izveido datuma objektu
            DateTime datums = new DateTime(gadaNummurs, menesaNummurs, dienasNummurs);

            //3.Nolasit DayOfWeek no datuma
            // int dayOfWeek = (int)datumu.DayOfWeek;
            DayOfWeek dayOfWeek = datums.DayOfWeek;

            //4.
            switch(dayOfWeek)
            {
                case DayOfWeek.Monday:
                    Console.WriteLine("Si ir pirmdiena");
                    break;
                default:
                    Console.WriteLine("diena netika atrasta");
                    break;
            }

            if(dayOfWeek == DayOfWeek.Saturday || dayOfWeek == DayOfWeek.Sunday)
            {
                Console.WriteLine("Sis ir brivdienas");
            }
            else
            {
                Console.WriteLine("Sis ir darbadienas");
            }
            Console.ReadKey();
        }
    }
}
