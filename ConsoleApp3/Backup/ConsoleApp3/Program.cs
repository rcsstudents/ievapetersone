﻿using System;
using System.Collections.Generic;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> userAnswer = new List<string>();
            int score = 0;

            // uzdodam jautajumu
            foreach (QuizQuestion question in DefinedQuestion.quizQuestion)
            {
                Console.WriteLine(question.Question);
                string answer = Console.ReadLine();
                if (answer == question.Answer)
                {
                    score++;
                }
                userAnswer.Add(answer);
            }

            // izdrukajam atbildes
            for (int i = 0; i < userAnswer.Count; i++)
            {
                Console.WriteLine("Pareiza atbilde: " + DefinedQuestion.quizQuestion[i].Answer);
                Console.WriteLine("Tu atbildeji: " + userAnswer[i]);
            }

            Console.WriteLine("Rezultats ir: " + score);
            Console.WriteLine("Max punktu skaits: " + userAnswer.Count);

            Console.ReadKey();
        }
    }
}
