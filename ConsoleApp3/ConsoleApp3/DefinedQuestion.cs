﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp3
{
    public static class DefinedQuestion
    {
        public static List<QuizQuestion> quizQuestion = new List<QuizQuestion>
        {
            new QuizQuestion()
            {
                Question = "Kāda ir tava mīļākā programmēšanas programma?",
                Answer = "C#"
            },
            new QuizQuestion()
            {
                Question = "Kāda ir tava mīļākā krāsa?",
                Answer = "Balta"
            },
            new QuizQuestion()
            {
                Question = "Kāda ir tava mīļākā TV programma?",
                Answer = "Fox"
            }
        };
    }
}