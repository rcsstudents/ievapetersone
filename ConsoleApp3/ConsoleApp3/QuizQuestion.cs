﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp3
{
    public class QuizQuestion
    {
        public string Question { get; set; }
        public string Answer { get; set; }
    }
}
